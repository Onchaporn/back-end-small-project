const mongoose = require('mongoose')
const Schema = mongoose.Schema
const customerSchema = new Schema({
  name: String,
  address: String,
  dob: Date,
  gender: String,
  mobile: String,
  email: String
})

const Customers = mongoose.model('Customers', customerSchema)

Customers.find(function (err, customers) {
  if (err) return console.error(err)
  console.log(customers)
})

module.exports = mongoose.model('Customers', customerSchema)
