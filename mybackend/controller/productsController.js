const Product = require('../models/Product')
const productController = {
  async addProduct (req, res, next) {
    const payload = req.body
    console.log(payload)
    const product = new Product(payload)
    try {
      await product.save()
      res.json(product)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateProduct (req, res, next) {
    const payload = req.body
    try {
      const product = await Product.updateOne({ _id: payload._id }, payload)
      res.json(product)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteProduct (req, res, next) {
    const { id } = req.params
    // res.json(usersController.deleteUser(id))
    try {
      const product = await Product.deleteOne({ _id: id })
      res.json(product)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getProducts (req, res, next) {
    try {
      const products = await Product.find({})
      res.json(products)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getProduct (req, res, next) {
    const { id } = req.params
    try {
      const product = await Product.findById(id)
      res.json(product)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = productController
