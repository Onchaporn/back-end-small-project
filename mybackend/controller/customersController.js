const Customer = require('../models/Customers')
const customerController = {
  lastId: 3,
  async addCustomer (req, res, next) {
    const payload = req.body
    console.log(payload)
    const customer = new Customer(payload)
    try {
      await customer.save()
      res.json(customer)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateCustomer (req, res, next) {
    const payload = req.body
    try {
      const customer = await Customer.updateOne({ _id: payload._id }, payload)
      res.json(customer)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteCustomer (req, res, next) {
    const { id } = req.params
    // res.json(usersController.deleteUser(id))
    try {
      const customer = await Customer.deleteOne({ _id: id })
      res.json(customer)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCustomers (req, res, next) {
    try {
      const customers = await Customer.find({})
      res.json(customers)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCustomer (req, res, next) {
    const { id } = req.params
    try {
      const customer = await Customer.findById(id)
      res.json(customer)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = customerController
